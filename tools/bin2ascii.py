#!/usr/bin/env python3
#
# torsten.roehl@fsg-preetz.org
# series hacking: the raspberry has you
# @desc: bin2ascii -b binaryInput  (convert to ascii)
#        bin2ascii -a asciiIinput  (convert to binary)

import sys

def bin2ascii( arg ):
    n = int(sys.argv[2].replace(' ', ''), 2)
    res =n.to_bytes((n.bit_length() + 7) // 8, 'big').decode()
    return res

def ascii2bin( arg ):
    res =(bin(int.from_bytes(sys.argv[2].encode(), 'big')))[2:]
    return res


if __name__ == "__main__":
    if len(sys.argv) != 1 and len(sys.argv) <= 3:
        if sys.argv[1] == "-b":
            print( bin2ascii( sys.argv[2]) )
        if sys.argv[1] == "-a":
            print( ascii2bin( sys.argv[2] ))

