#!/usr/bin/env python3

#
# q&d solution arm assembly formatter 0.1
# @author torsten roehl
# @since  01.02.2018
#
# using style form MODERN ASSEMBLY LANGUAGE PROGRAMMING WITH THE ARM PROCESSOR
# by Larry D. Pyeatt

VERSION="asfo v0.1"

import sys

################################################################################
#     armFormatter                                                             #
################################################################################
def armFormatter(pathFile):
    
    mulCom = False # for multiline comments

    f=open(pathFile)
    for line in f:
        l=' '.join(line.split())
        #
        # do nothing in multiline comments
        #
        if( mlStartComment(l) ):
            mulCom = True
            # keine korrektur
            print(line.rstrip())
            continue

        if( mulCom):
            if( mlEndComment(l)):
                mulCom = False
            print(line.rstrip())
            continue

        #
        # skip string
        #
        if l.find("\"") != -1:
            ll = line.rstrip();
            ll = ll.lstrip();
            print("\t" + ll );
            
            continue
        #
        # formatting labels, directive, instruction and
        # singleline comments
        #

        v=l.split(" ")
        length=len(v)
        
        # directive
        if(isDirective(v[0])):
            v[0]='\t' + v[0]
        
        # label 
        elif(isLabel(v[0])):
           if(length > 1):
                if(v[1].startswith('.')):
                   v[1]='\n\t'+v[1]
        
        # instruction
        elif(isInstruction(v[0])):
            v[0]='\t' + v[0]
            if(length > 1):
                    v[1]='\t'+v[1]

        # comments
        l= ' '.join(v)
        l=comments(l)
        print(l)

    f.close()


###############################################################################
#       multilineComment                                                      #
###############################################################################
def mlStartComment(zeile):
     res = False

     if(zeile.startswith('/*') and not zeile.endswith('*/')):
        res=True

     return res
###############################################################################
#       multilineComment                                                      #
###############################################################################
def mlEndComment(zeile):
     res = False

     if(not zeile.startswith('/*') and zeile.endswith('*/')):
        res=True

     return res


###############################################################################
#           isLabel                                                           #
###############################################################################
def isLabel(value):
    res=False
    
    if(value.endswith(':')):
        res=True
    
    return res

###############################################################################
#        isDirective                                                          #
###############################################################################
def isDirective(value):
    res=False
    
    if(value.startswith('.')):
        res=True
    
    return res

###############################################################################
#       isInstruction                                                         #
###############################################################################

def isInstruction(value):
    res=True
    
    if(value.startswith('.')):
        res=False
        
    if(value.endswith(':')):
        res=False

    if(value.endswith('*/')):
        res=False

    if(value.startswith('/*')):
        res=False

    if(value.startswith('//')):
        res=False
   
    if(value.startswith('@')):
        res=False
   
    return res
################################################################################
#         comment                                                              #
################################################################################
def comments(value):
    res=value
    # arm style comments
    res=res.replace("@" ,"\t@")
    # c++ style comments
    res=res.replace("//","\t//")
    # one line c-style comments
    if res.find("/*"):
        if res.find("*/"):
            res=res.replace("/*","\t/*")
    
    return res 

################################################################################
#            MAIN                                                              #
################################################################################
def main(argv):

    if( len(sys.argv) != 2 ):
        print(VERSION)
        print( 'Usage: ./asfo pathFileName')
        print( 'missing parameter!')
        sys.exit(0)
    
    armFormatter( sys.argv[1])
################################################################################
if __name__ == '__main__':
    main(sys.argv)

