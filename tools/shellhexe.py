#!/usr/bin/env python3


# Wed 21 Feb 15:42:59 UTC 2018
# @author torsten.roehl
# @desc   simpletool to create shellcode output from given hexaddress
#         (and vice versa)
VERSION="shellhexe v.01"

import getopt
import sys
import re
###############################################################################
#  main method to create shellcode output from given hexaddr string
###############################################################################

def hex2shell(strHex,littleEndian):

    tmp=strHex
    endian=littleEndian
    ls=[]

    # step 0: create raw data list based on "0x"
    indices = [0]
    # all matches for the expression 0x## (with 1-4 ##'s)
    matches = re.finditer(r'(0x([0-9a-fA-F](0(?!x)|[1-9a-fA-F])){1,4})', tmp)
    # append indices from the matches to the list
    for m in matches:
        indices.extend((m.start(0), m.end(0)))
    # split string at the indices
    ls = [tmp[i:j] for i,j in zip(indices, indices[1:]+[None])]

    # step 1: convert 0x strings into shellcode style
    for i in range(len(ls)):
        if ls[i].startswith('0x'):
            # without 0x [2:]
            s = asShellcode(ls[i][2:],endian)
            ls[i] = s

    # step 2: convert list to string
    res = ''.join(ls)

    return res

################################################################################
#   main method to create hexaddr output from given shellcode       #
################################################################################
def shell2hex(strShell,littleEndian):
    
    tmp=strShell
    endian=littleEndian
    ls=[]

    # step 0:
    indices = [0]
    # all matches for the expression \x__ with "_" from 0-f
    matches = re.finditer(r'((\\x[0-9a-fA-F][0-9a-fA-F])+)', tmp)
    # append indices from the matches to the list
    for m in matches:
        indices.extend((m.start(0), m.end(0)))
    # split string at the indices
    ls = [tmp[i:j] for i,j in zip(indices, indices[1:]+[None])]
    
    # step 1:
    for i in range(len(ls)):
        if ls[i].startswith("\\x"):
             # without 0x [2:]
             s = asAddress( ls[i][2:],endian)
             ls[i]=s

    # step 2: convert list to string
    res = ''.join(ls)
    return res

    

########################################################
#  convert a given hexaddress to shellcode string      #
########################################################
def asShellcode(strAddr, endian):
   
    tmp= strAddr.replace("0x",'');
   
    # split in a list of length two
    l=lengthSplit(tmp,2)
    # little endian?
    if(endian):
        line = l.reverse() 	
     
    # add \x and join
    line = [ "\\x"  + x  for x  in l ]
    s = ''.join(line)

    return s

##########################################################################
#    convert a shellcode address \x00\x11\x22\x33 to hexaddr: 0x00112233 #
##########################################################################
def asAddress(strShell, endian):

    tmp = strShell 
    tmp = tmp.replace("\\x",'')

    # split in a list of length two
    l=lengthSplit(tmp,2)

    # little endian? 
    if(endian):
        l.reverse()  
  
    # join and add 0x 
    s = ''.join(l)
    s = "0x" + s
    #print(s)    
    return s
###########################################################################
def usage():
    print(VERSION)   
    print("usage:  [options] arg1 ")
    print("")
    print("options:")
    print("-h, --help          show this help message and exit")
    print("-l  [default]       output in little endian format")
    print("-b                  output in big endian format")
    print("-a STRING           convert hexaddress to shellcode")
    print("-s STRING           convert shellcad to hexaddress")
    print("")
    print("-l cannot occur with -b together")
    print("-s cannot occur with -a together")
    sys.exit()               
###############################################################################
#  split a string in a list based on the parameter length                     #
###############################################################################
def lengthSplit(input, length):
    return [input[start:start+length] for start in range(0, len(input), length)]

###############################################################################
# isAllowed helper method for getopt                                          #
###############################################################################
def isAllowed(ls):
    dic = dict(ls)
    
    if "-l" in dic and "-b" in dic:
        return False

    if "-a" in dic and "-s" in dic:
        return False

    return True
###############################################################################
#                   main                                                      #
###############################################################################
def main(argv):
    if len(argv) == 1:
        usage()
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hlbs:a:", ["help"])
    except getopt.GetoptError as err:
        print(err) # will print something like "option -a not recognized"
        usage()
    
    endian = True
    


    if not isAllowed(opts):
        usage() # and exit!
 
    for o, a in opts:
        if o =="-b":
        	 endian = False
        elif o == "-l":
             endian = True
        elif o in ("-h", "--help"):
             usage()
        elif o == "-s":
             print( shell2hex(a,endian))
        elif o == "-a":
             print( hex2shell(a,endian))
        else:
             assert False, "unhandled option"
   
################################################################################
if __name__ == '__main__':
    main(sys.argv)

