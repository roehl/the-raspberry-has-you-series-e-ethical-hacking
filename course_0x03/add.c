#include <stdio.h>            // because of printf
#include <stdlib.h>           // because of atoi

int main(int argc, char *argv[])
{
      int number;
      if (argc != 3)
                return 1;
         
      number = atoi(argv[1]) + atoi(argv[2]); 
      printf("%d\n", number);
          
     return 0;
}
