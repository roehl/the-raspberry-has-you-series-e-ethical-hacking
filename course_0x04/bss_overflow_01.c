/*
 * bss overflow example
 * @source kunst des penetration testing (Ruef)
 * @usage erfordert (mini) exploit!
 */
#include <stdio.h>
#include <string.h>

int DEBUG;

int main( int argc, char *argv[]){

	/* die buffer werden im bss segment angelegt!
	 */
	static char std_buffer[12];
	static char secret_buffer[12];

	if( argc != 2){
		printf( "Usage: %s <string>\n",argv[0]);
		return 1;
	}

	printf("BSS Adresse (std_buffer):\t %p\n",std_buffer);
	printf("BSS Adresse (secret_buffer):\t %p\n",secret_buffer);

	strcpy(std_buffer, argv[1]);
	strcpy(secret_buffer, "geheim");
    
	int len = strlen(argv[1]);
	printf("\nInhalt (std_buffer):\t %s  (%d)\n",std_buffer,len);

	if(DEBUG==0x2a){
		printf("\nDie folgende Zeile ist Top Secret!\n");
		printf("Inhalt (secret_buffer):\t %s\n",secret_buffer);
	}
}
