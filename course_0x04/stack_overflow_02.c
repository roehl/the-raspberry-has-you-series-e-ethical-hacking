#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void geheimeFunktion(){
    puts("Ich werde niemals aufgerufen!");
    exit(0);
}

void vulnerable(char *text){
    char buffer[10];
    strcpy(buffer,text);

}

int main(int argc, char** argv){

	if(argc != 2)
		return -1;

    vulnerable(argv[1]);
    return 0;

}
