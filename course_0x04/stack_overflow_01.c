/*
 * 
 * @source: the art of exploitation (Erickson)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int check_authentication(char *password) {
	int res = 0;
	char password_buffer[16];

	strcpy(password_buffer, password);
	
	if(strcmp(password_buffer, "jedi") == 0)
		res = 1;
	if(strcmp(password_buffer, "sith") == 0)
		res = 1;

	return res;
}


int main(int argc, char *argv[]) {

	if(argc < 2) {
		printf("Usage: %s <password>\n", argv[0]);
		exit(0);
	}

	if(check_authentication(argv[1])) {
		printf("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
		printf("      Access Granted.\n");
		printf("-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
	} else {
		printf("\nAccess Denied.\n");
   }

}
	
