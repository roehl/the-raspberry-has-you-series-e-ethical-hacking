/*
 *
 * off-by-one overflow 
 * Programm funktioniert, obwohl ein off-by-one-Fehler vorhanden ist!
 *
 */
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {

	char buffer[12];

	if( argc < 2 ){
         printf("usage: ./off_by_one_overflow_01 password\n");
         printf("password length:  maximal 12 character!\n");
		 return 0;
	}


    // Eingabe (wird in buffer abgelegt)
	int i;
	for (i = 0; i <= 12; i++) 
          buffer[i] = argv[1][i];

	// Ausgabe (ein wenig umstaendlich...)
	int len = strlen(argv[1]);

	printf("\nIhre Eingabe war: ");
	for(i = 0; i < len;i++){
		if( i < 12)
              printf("%c",buffer[i]);
	}
	printf("\n");


	return 0;
}
