/*
 * off-by-one overflow example
 * usage : ohne parameter wird ls aufgerufen
 * usage : mit  parameter .... kann auch ps aufgerufen werden!
 * usage : exploit als parameter!
 */
#include <stdlib.h>
#include <stdio.h>
	
int main(int argc, char **argv) {


	char *process;
	char *list;
	char buffer[12];
	int i;

	process = "/bin/ps";
	list = "/bin/ls";

	/* read input */
	if( argc > 1 ){
		/* overflow */
		for (i = 0; i <= 12; ++i) 
            buffer[i] = argv[1][i];
	}

    /* output */
	printf("Adresse  process: %p\n",process);
	printf("Adresse  list: %p\n",list);
   	
	printf("\nAusgabe Kommando:\n");
	system(list);
	
	return 0;
}
