/*
 * gcc -o addrc addrc.c -ldl
 * NOTE: -ldl is needed!
 */
#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char *argv[])
{
  void *self = dlopen(NULL, RTLD_NOW);
 
  printf("system(): %p\n", dlsym(self, "system"));
  printf("seed48(): %p\n", dlsym(self, "seed48"));
  printf("printf(): %p\n", dlsym(self, "printf"));
  printf("exit(): %p\n", dlsym(self, "exit"));
 
  return 0;
}
