/*chapter TODO -return to zero protection  
 *compile with: gcc -g -o ret2zp ret2zp.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void vulnerable(char *puffer){
	char buffer[5];
	strcpy(buffer,puffer);
}

int main(int argc, char **argv) {

	vulnerable(argv[1]);

	printf("argv[0] Programm:\t%s\n", argv[0]);
	printf("argv[1] Parameter:\t%s\n", argv[1]);

	return 0;
}

