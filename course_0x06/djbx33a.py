#!/usr/bin/python
import sys
# DJBX33A Hash-Algorithmus 
# (Daniel J. Bernstein, Times 33 with Addition) 
#
#

def djbx33a(plaintext):
    hash = 5381
    for i in range(len(plaintext)):
        hash = hash*33 + ord(plaintext[i])
    
    return hash


if __name__ == "__main__":
    if len(sys.argv) == 2:
        s=sys.argv[1]
        print( djbx33a(s) )
    else:
        print( "[usage]: djbx33a plaintext")
