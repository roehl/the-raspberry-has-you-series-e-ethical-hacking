#!/usr/bin/python3
# course example: using of sha512 crypt in python
# linux commandline: mkpasswd --help  and mkpasswd -m --help
# python: crypt

import crypt
import random
import string

# random salt
# salz = ''.join(random.sample(string.ascii_letters,8))

prefix = '$6$'
passwd = 'hallo'
salz = "aabbccdd"

value=crypt.crypt(passwd, prefix + salz) 

print(value)
