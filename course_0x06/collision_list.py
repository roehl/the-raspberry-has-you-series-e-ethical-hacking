#!/usr/bin/python3
import sys
import string
import random
# DJBX33A Hash-Algorithmus 
# (Daniel J. Bernstein, Times 33 with Addition) 
#


def djbx33a(plaintext):
    hash = 5381
    for i in range(len(plaintext)):
        hash = hash*33 + ord(plaintext[i])
    
    return hash

def newString(length):
 ## Just alphanumeric characters
    chars = string.ascii_letters + string.ascii_letters
    pwdSize = length

    return ''.join((random.choice(chars)) for x in range(pwdSize))

if __name__ == "__main__":
    for i in range(10000):
        s = newString(2)
        print(  str(djbx33a(s)) + "\t" + s )

