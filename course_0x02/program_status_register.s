/*
 * @author torsten.roehl@fsg-preetz.org
 * @course series: the raspberry has you
 * @program
 *         study the current program status register
 *         here we invesigate the carry and overflow flag
 *
 * i)   signed   value + signed   value -> overflow but no carry!
 * ii)  signed   value + unsigned value -> carry but no overflow!
 * iii) unsigned value + unsigned value -> carry but no overflow!
 * 
 * -----------------------------------
 * | N | Z | C | V |27 - 5 |1|0|0|0|0|  CPSR
 * -----------------------------------
 *  31  30  29   28         4 3 2 1 0   BITS
 */
	
	.global _start
	
_start:
/* no overflow and no carry */
	bl 	clear_flags
/* overflow and no carry */
	ldr 	r0, =0x7fffffff 	@range of signed value
	ldr 	r1, =0x00000001
	adds 	r0, r1, r0
	
	bl 	clear_flags
/* carry and no overflow */
	ldr 	r0, =0xffffffff
	ldr 	r1, =0x00000001
	adds 	r0 , r1, r0
	
	bl 	clear_flags
/* exit */
	mov 	r0, #0
	mov 	r7, #1
	svc 	#0
	
/* clear nzcv */
clear_flags:
    
	mov 	r0, #1
	adds 	r0,r0,#0
	bx 	lr
