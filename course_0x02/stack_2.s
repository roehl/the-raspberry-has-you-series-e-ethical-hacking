/*
Mnemonics for:
*/

        .global _start

_start:
    /* Save sp before push. */
    mov r0, sp

    /* push */
        mov r1, #11
	mov r2, #22
	mov r3, #33
	mov r4, #44
	push {r1}
	mov r0, sp
    	push {r2,r3,r4}
	mov r0, sp


    /* pop */
    mov r6, #0
    mov r7, #0
   
    pop {r6}
    mov r0, sp

	pop {r7}
	mov r0,sp


    pop {r6, r7}
    
	mov r0, sp


/* exit */
	mov r0, #0
	mov r7, #1
	svc  #0
