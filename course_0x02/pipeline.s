/******************************************************************************
 * @course series: the raspberry has you reverse engineering                  *
 * @author:        torsten.roehl@fsg-preetz.org                               *
 * @version:                                                                  *
 * @usage  :  gdb session with  info register to stude pipeline and pc        * 
 ******************************************************************************/

        .global _start

_start:
    mov r0, pc
    mov r1, pc
    mov r0, pc
    mov r1, pc
    mov r0, pc
    mov r1, pc
    mov r0, pc
    mov r1, pc

/* exit */
	mov r0, #0
	mov r7, #1
	svc  #0
