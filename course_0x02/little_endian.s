/******************************************************************************
 * @course series: the raspberry has you reverse engineering                  *
 * @author:        torsten.roehl@fsg-preetz.org                               *
 * @version:                                                                  *
 *                                                                            *
 ******************************************************************************/
	.global _start
_start:
	
	mov 	r2,#25
	mov 	r3,#15
	add 	r4,r2,r3
	sub 	r5,r2,r3
	
	mov 	r0,#0
	mov 	r7,#1
	svc 	0
