#include <stdio.h>

// multiplies two integers
int func2(int a, int b){
    return a * b;
}

// multiplies integer by 2
int func1(int a){
    int b = 2;
    return func2(a, b);
}

int main(){
    int x = func1(0x31);
    printf( "Our number is 0x%x\n", x );
    return 0;
}

