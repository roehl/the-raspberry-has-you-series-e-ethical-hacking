@ standard hello world programm in arm assembly

			@ How to compile and execute?
			@ step 0: as -o hello_world.o hello_world.s
			@ step 1: ld -o hello_world   hello_world.o
			@ step 2: ./hello_world

	.global _start

_start:

        		/* print */
	mov r0, $1	@ output to konsole
 	ldr r1, =str 	@ output string
 	ldr r2, =len
	mov r7, $4	@ syscall number to write
	svc 0		@ execute syscall

			/* exit */
	mov r0,#0	@ exit status 
	mov r7,#1	@ syscall number to exit
	svc 0		@ execute syscall

	.data
str:
	.asciz "Hello World!\n"
	len = .-str  	@ 13
