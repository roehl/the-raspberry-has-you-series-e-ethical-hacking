# The Raspberry Has You (Einstiegskurs)

![ - Tutorial - ](http://fablab-wiki.fsg-preetz.de/cover.png)
## Ethical Hacking and Reverse Engineering

#Tipp:

    git clone https://roehl@bitbucket.org/roehl/the-raspberry-has-you-series-e-ethical-hacking.git series_hacking
### LINKS

Tutorials

* THINK IN GEEK http://thinkingeek.com/arm-assembler-raspberry-pi/

* AZERIA LABS https://azeria-labs.com/

ARM-Informationen

* ARM Infocenter http://infocenter.arm.com/help/index.jsp

* ARM Linux Developer http://www.arm.linux.org.uk/developer

Linux-Informationen

* Linux syscalls https://w3challs.com/syscalls/?arch=arm_strong

* Linux mangages http://www.manpages.info/

BSI (Bundesamt für Sicherheit in der Informationstechnik) 

* Cyber-Sicherheit https://www.bsi.bund.de/DE/Themen/Cyber-Sicherheit/Dienstleistungen/ISPentest_ISWebcheck/ispentest_iswebcheck_node.html;jsessionid=2EE865493E7638EBADB6FA1718A21739.1_cid360


weitere Quellen

* DAVESPACE  http://www.davespace.co.uk/arm/introduction-to-arm/
* C/LIBC Dokumentation (de)   https://www.proggen.org/
* Shellcodes database for study cases   http://shell-storm.org/shellcode/
* Ethical Hacking Tutorial https://www.tutorialspoint.com/ethical_hacking/index.htm
* Dictionary/Wordlists http://www.md5this.com/tools/wordlists.html
* Exploit Database https://www.exploit-db.com/platform/?p=ARM

CTF und WARGAMES

* Bandit https://overthewire.org/wargames/
* Toddler'S Bottle (Anfänger) http://pwnable.kr/play.php  (siehe YouTube: https://youtu.be/971eZhMHQQw )
* Übersicht über weitere CTFs und Wargmes

      * https://wheresmykeyboard.com/2016/07/hacking-sites-ctfs-wargames-practice-hacking-skills/    
      * https://www.checkmarx.com/2015/11/06/13-more-hacking-sites-to-legally-practice-your-infosec-skills/


Kryptologie

* https://www.cryptool.org/de/ (freies Ebook und Software)

Tools

* ARM Syntax Vim https://github.com/ARM9/arm-syntax-vim

    VIM

    * Sammlung von Plugins https://vimawesome.com/
    * http://vimcasts.org (Videos ausgewählter Themen)
    * http://www.vimgenius.com (interaktives Tutorial/Übungen)
    * TheFrugalComputerGuy http://thefrugalcomputerguy.com/linux/Vim/index.php
    
    Seiten mit Übersichten und Tipps

    * https://www.cs.oberlin.edu/~kuperman/help/vim/home.html
    * https://www-user.tu-chemnitz.de/~hot/VIM/VIM/Tipps_und_Tricks.html


### C Programme kompiliern und ausfuehren:

    step 0: gcc -g -o hello_word hello_word.c
    step 1: ./hello_word


### ARM Assemblerprogramm kompilieren und ausfuehren:

    step 0: as -g -o hello_world.o hello_world.s
    step 1: ld -g -o hello_world   hello_world.o
    step 2: ./hello_world