#!/usr/bin/python3
#
# basic socket example (source: grey hat hacking 4ed)
# [usage] step 0: nc -l -p 4711
#         step 1: run this script :-)

import socket
#step 0: create socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', 4711))
# step 1: send message
text='Hello, Hacking-Course'.encode('utf-8')
s.sendall( text)  
# step 2: receive message
data = s.recv(1024)
# step 3: close socket
s.close()

print( 'Received', 'data')
