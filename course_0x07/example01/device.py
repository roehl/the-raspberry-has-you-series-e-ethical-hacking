#!/usr/bin/python

import sys

DEVICE_TYPE = 'DEV_VIRTUAL'
#DEVICE_TYPE = 'DEV_REAL'

def device(command):

    if DEVICE_TYPE == 'DEV_VIRTUAL':
        device_virtual(command)
    
    if DEVICE_TYPE == 'DEV_REAL':
        device_real(command)



def device_real(command):
    print("...calling real device")
    print("...real device not available")

def device_virtual(command):
    print("...calling virtual device")
    if command == 'open':
        print("... device is now open")
    if command == 'close':
        print("... device is now close")


def main(argv):
    if( len(sys.argv) != 2):
        print("missing parameter!")
        sys.exit(0)

    device( sys.argv[1] )




if __name__ == '__main__':
    main(sys.argv)
