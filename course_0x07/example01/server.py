#!/usr/bin/python

#
# this example is based on: https://hg.python.org/cpython/file/3481c6f36e55/Lib/SimpleHTTPServer.py
#

import os
import posixpath
import BaseHTTPServer
import urllib
import cgi
import sys
import shutil
import mimetypes
import urlparse
from cStringIO import StringIO

import subprocess

HOST = '' # NOT USED HERE!
PORT = 8888;
DIRECTORY = 'www' # Main HTML-Directory (including index.html )


class SimpleWebServer(BaseHTTPServer.BaseHTTPRequestHandler):
       

    def do_GET(self):
               
        cmd = urlparse.urlparse(self.path).query 
        parsed = urlparse.urlparse(self.path)
        params = urlparse.parse_qsl(parsed.query) 
        
                 
        if cmd: # parameter vorhanden?
			self.data_manager(params)   
        
        f = self.send_head()  
        if f:
            self.copyfile(f, self.wfile)
            f.close() 



    def data_manager(self,params):
	#	print ("status: received command:", params)
                command = ''
		for x,y in params:
	#		print ("Parameter = "+x,"Value = "+y)
                        command += ' ' + y
                
                print ("status:  ...try command ", command )
                subprocess.call( command, shell=True )


    def log_message(self, format, *args):
		 # do nothing
		 return
		 
    def address_string(self):
		host, port = self.client_address[:2]
		#return socket.getfqdn(host)
		return host
      
    def do_POST(self):
		cmd = urlparse.urlparse(self.path).query  
		print ("status: received : ", cmd) 
		
    def do_HEAD(self):       
        
        f = self.send_head()
        if f:
            f.close()

    def send_head(self):
     
        path = self.translate_path( DIRECTORY +self.path)       
        f = None

        if os.path.isdir(path):
        
            for index in "index.html":

                index = os.path.join(path, index)
                if os.path.exists(index):
                    path = index
                    break            

        ctype = self.guess_type(path)

        try:
            f = open(path, 'rb')

        except IOError:

            self.send_error(404, "File not found")
            return None

        self.send_response(200)
        self.end_headers()
     #   self.wfile.write('{"status" : "ready"}')
        
        return f


    def translate_path(self, path):
        path =  path
        # abandon query parameters            
        path = path.split('?',1)[0]            
        path = path.split('#',1)[0]     
                
        
        # Don't forget explicit trailing slash when normalizing. Issue17324

        trailing_slash = path.rstrip().endswith('/')
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = os.getcwd()

        for word in words:

            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        if trailing_slash:

            path += '/'

        return path

    def copyfile(self, source, outputfile):        
        shutil.copyfileobj(source, outputfile)

    def guess_type(self, path):
   
        base, ext = posixpath.splitext(path)

        if ext in self.extensions_map:
            return self.extensions_map[ext]

        ext = ext.lower()
        if ext in self.extensions_map:
            return self.extensions_map[ext]

        else:
            return self.extensions_map['']


    if not mimetypes.inited:
        mimetypes.init() # try to read system mime.types

    extensions_map = mimetypes.types_map.copy()
    extensions_map.update({
        '': 'application/octet-stream',
        '.py': 'text/plain',
        '.c': 'text/plain',
        '.h': 'text/plain',
        })   
        
def main():
	server_class = BaseHTTPServer.HTTPServer
	httpd = server_class((HOST, PORT), SimpleWebServer)

	try:
		print("status: server on port: ", PORT , " ... running " ) 
		httpd.serve_forever()
	except KeyboardInterrupt:
		pass
		
	print("status: server on port: ", PORT , " ... close " ) 
	httpd.server_close()

if __name__ == '__main__':
    main()
